import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:oneallmarketscreens/view/product_detail/edit_product_screen.dart';
import 'package:oneallmarketscreens/view/profile/profile_view.dart';
import 'package:oneallmarketscreens/view/search/search_view.dart';
import 'package:oneallmarketscreens/view/sign_in/sign_in_view.dart';

import 'view/sign_up/sign_up_view.dart';

void main() {
  SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
    statusBarColor: Colors.transparent, // transparent status bar
  ));
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      // Hide the debug banner
      debugShowCheckedModeBanner: false,
        title: 'OneAllMarket',
      home: const SignInView(),
      // Register routes
      routes: {
        '/addProduct-screen': (BuildContext ctx) =>  EditProductScreen(),
        '/profile-screen': (BuildContext ctx) => const ProfileView()
      },
    );
  }
}