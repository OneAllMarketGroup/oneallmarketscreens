import 'package:flutter/material.dart';

class MyDrawer extends StatelessWidget {
  const MyDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      elevation: 6,
      child: Column(children: [
        const SizedBox(height: 100),
        ListTile(
          onTap: () {
            Navigator.of(context).pushReplacementNamed('/');
          },
          leading: const Icon(Icons.home),
          title: const Text(
            'Home',
            style: TextStyle(fontSize: 20),
          ),
        ),
        ListTile(
          onTap: () {
            Navigator.of(context).pushReplacementNamed('/addProduct-screen');
          },
          leading: const Icon(Icons.settings),
          title: const Text(
            'Add Product',
            style: TextStyle(fontSize: 20),
          ),
        ),
        ListTile(
          onTap: () {
            Navigator.of(context).pushReplacementNamed('/profile-screen');
          },
          leading: const Icon(Icons.contact_mail),
          title: const Text(
            'Profile Screen',
            style: TextStyle(fontSize: 20),
          ),
        ),
      ]),
    );
  }
}