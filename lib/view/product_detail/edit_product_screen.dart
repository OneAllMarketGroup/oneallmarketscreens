import 'package:flutter/material.dart';
import 'package:oneallmarketscreens/view/screens/home/home_screen.dart';
import 'package:provider/provider.dart';
import 'package:oneallmarketscreens/view/constants.dart';
import 'package:oneallmarketscreens/view/product_detail/provider/product.dart';
import 'package:oneallmarketscreens/view/product_detail/provider/products_provider.dart';
import 'package:oneallmarketscreens/view/drawer.dart';

import 'dart:io';
import 'package:image_picker/image_picker.dart';

class EditProductScreen extends StatefulWidget {

  static const routeName = '/edit-product';

  @override
  EditProductScreenState createState() => EditProductScreenState();
}

class EditProductScreenState extends State<EditProductScreen> {

  File? file;
  ImagePicker imagePicker = ImagePicker();

  getImage(ImageSource source) async {
    final file = await imagePicker.getImage(source: source);

    setState(() {
      this.file = File(file!.path);
    });
  }

  final _nameFocusNode = FocusNode();
  final _descriptionFocusNode = FocusNode();
  final _priceFocusNode = FocusNode();
  final _sellerFocusNode = FocusNode();
  final _stockFocusNode = FocusNode();
  final _locationFocusNode = FocusNode();

  final _form = GlobalKey<FormState>();

  var _isLoading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      drawer: const MyDrawer(),

      appBar: AppBar(
        backgroundColor: primaryColor,

        title: const Text('Add Product'),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.save),
            onPressed: () {},
          ),
        ],
      ),
      body: _isLoading
          ? const Center(
              child: CircularProgressIndicator(),
            )
          : Padding(
              padding: const EdgeInsets.all(16.0),
              child: Form(
                key: _form,
                child: ListView(
                  children: <Widget>[
                    TextFormField(
                     // initialValue: 'Name',
                      decoration: const InputDecoration(labelText: 'Name'),
                      textInputAction: TextInputAction.next,
                      onFieldSubmitted: (_) {
                        FocusScope.of(context).requestFocus(_nameFocusNode);
                      },
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Please provide a value.';
                        }
                        return null;
                      },
                      onSaved: (value) {
                      },
                    ),

                    TextFormField(
                     // initialValue:'Description',
                      decoration: const InputDecoration(labelText: 'Description'),
                      maxLines: 3,
                      keyboardType: TextInputType.multiline,
                      focusNode: _descriptionFocusNode,
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Please enter a description.';
                        }
                        if (value.length < 6) {
                          return 'Should be at least 6 characters long.';
                        }
                        return null;
                      },
                      onSaved: (value) {

                      },
                    ),

                    TextFormField(
                     // initialValue: 'Price',
                      decoration: const InputDecoration(labelText: 'Price'),
                      textInputAction: TextInputAction.next,
                      keyboardType: TextInputType.number,
                      focusNode: _priceFocusNode,
                      onFieldSubmitted: (_) {
                        FocusScope.of(context)
                            .requestFocus(_priceFocusNode);
                      },
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Please enter a price.';
                        }
                        if (double.tryParse(value) == null) {
                          return 'Please enter a valid number.';
                        }
                        if (double.parse(value) <= 0) {
                          return 'Please enter a number greater than zero.';
                        }
                        return null;
                      },
                      onSaved: (value) {

                      },
                    ),

                    TextFormField(
                      //initialValue:'Seller',
                      decoration: const InputDecoration(labelText: 'Seller'),
                      maxLines: 3,
                      keyboardType: TextInputType.multiline,
                      focusNode: _sellerFocusNode,
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Please enter a name of Seller.';
                        }
                        if (value.length < 4) {
                          return 'Should be at least 4 characters long.';
                        }
                        return null;
                      },
                      onSaved: (value) {

                      },
                    ),

                    TextFormField(
                      //initialValue: 'In Stock',
                      decoration: const InputDecoration(labelText: 'In Stock'),
                      textInputAction: TextInputAction.next,
                      keyboardType: TextInputType.number,
                      focusNode: _stockFocusNode,
                      onFieldSubmitted: (_) {
                        FocusScope.of(context)
                            .requestFocus(_descriptionFocusNode);
                      },
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Please enter a number.';
                        }
                        if (double.tryParse(value) == null) {
                          return 'Please enter a valid number.';
                        }
                        if (double.parse(value) <= 0) {
                          return 'Please enter a number greater than zero.';
                        }
                        return null;
                      },
                      onSaved: (value) {

                      },
                    ),

                    TextFormField(
                      //initialValue:'Location',
                      decoration: const InputDecoration(labelText: 'Description'),
                      maxLines: 3,
                      keyboardType: TextInputType.multiline,
                      focusNode: _locationFocusNode,
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Please enter a description.';
                        }
                        if (value.length < 3) {
                          return 'Should be at least 3 characters long.';
                        }
                        return null;
                      },
                      onSaved: (value) {

                      },
                    ),

                    Container(
                      alignment: Alignment.center,
                      // height: 200,
                      width: MediaQuery.of(context).size.width,
                      child: Column(
                        children: [
                          SizedBox(
                            height: 20,
                          ),
                          Container(
                            child: file == null
                                ? Container(
                              height: 200,
                              width: 300,
                              color: primaryColor,
                            )
                                : Container(
                              height: 200,
                              width: 300,
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: FileImage(file!),
                                ),
                              ),
                            ),
                          ),

                          SizedBox(
                            height: 20,
                          ),

                          Container(
                            height: 60,
                            width: 250,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                ElevatedButton(

                                  onPressed: () {
                                    getImage(ImageSource.camera);

                                  },
                                  child: Text('Camera'),

                                ),
                                ElevatedButton(
                                  onPressed: () {
                                    getImage(ImageSource.gallery);
                                  },
                                  child: Text('Gallery'),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),


                    const SizedBox(height: defaultPadding * 2),
                    Center(
                      child: SizedBox(
                        width: MediaQuery.of(context).size.width * 0.9,
                        height: 50,
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            primary: Color(0xff0ACF83),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                          ),
                          onPressed: () {
                            Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(builder: (context) => HomeScreen()),
                            );
                          },
                          child: Text('Add To Cart'),
                        ),
                      ),
                    )




                  ],
                ),
              ),
            ),
    );
  }
}
