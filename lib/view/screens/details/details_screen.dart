import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:oneallmarketscreens/view/screens/models/Product.dart';
import 'package:oneallmarketscreens/view/constants.dart';
import 'package:oneallmarketscreens/view/shopping_cart/shopping_cart_view.dart';
import 'package:oneallmarketscreens/view/drawer.dart';

import 'components/color_dot.dart';

class DetailsScreen extends StatelessWidget {
  const DetailsScreen({Key? key, required this.product}) : super(key: key);

  final Product product;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: product.bgColor,

      appBar: AppBar(
        backgroundColor: primaryColor,
        title: Text(
            'Product Details'
        ),
        leading: const BackButton(color: Colors.black),
        actions: [
          IconButton(
            onPressed: () {},
            icon: CircleAvatar(
              backgroundColor: Colors.white,
              child: SvgPicture.asset(
                "assets/icons/Heart.svg",
                height: 20,
              ),
            ),
          )
        ],
      ),
      body: Column(
        children: [
          Image.asset(
            product.image,
            height: MediaQuery.of(context).size.height * 0.4,
            fit: BoxFit.cover,
          ),
          const SizedBox(height: defaultPadding * 1.5),
          Expanded(
            child: Container(
              padding: const EdgeInsets.fromLTRB(defaultPadding,
                  defaultPadding * 2, defaultPadding, defaultPadding),
              decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(defaultBorderRadius * 3),
                  topRight: Radius.circular(defaultBorderRadius * 3),
                ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: Text(
                          product.title,
                          style: Theme.of(context).textTheme.headline6,
                        ),
                      ),
                      const SizedBox(width: defaultPadding),
                      Text(
                        "\$" + product.price.toString(),
                        style: Theme.of(context)
                            .textTheme
                            .headline6!
                            .copyWith(
                            color: primaryColor,
                            fontSize: 16,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),

                  Divider(
                    height: 0,
                    color: Color(0xff7F7F7F),
                  ),

                  Row(
                    children: [
                      Expanded(
                        child: Text(
                          "Seller :",
                          style: Theme.of(context).textTheme.headline6,
                        ),
                      ),
                      const SizedBox(width: defaultPadding),
                      Text(
                        "Thomas",
                        style: Theme.of(context)
                            .textTheme
                            .headline6!
                            .copyWith(
                            color: primaryColor,
                            fontSize: 13,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),

                  Divider(
                    height: 0,
                    color: Color(0xff7F7F7F),
                  ),

                  Row(
                    children: [
                      Expanded(
                        child: Text(
                          "In Stock :",
                          style: Theme.of(context).textTheme.headline6,
                        ),
                      ),
                      const SizedBox(width: defaultPadding),
                      Text(
                        "20",
                        style: Theme.of(context)
                            .textTheme
                            .headline6!
                            .copyWith(
                            color: primaryColor,
                            fontSize: 13,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),

                  Divider(
                    height: 0,
                    color: Color(0xff7F7F7F),
                  ),

                  Row(
                    children: [
                      Expanded(
                        child: Text(
                          "Location :",
                          style: Theme.of(context).textTheme.headline6,
                        ),
                      ),
                      const SizedBox(width: defaultPadding),
                      Text(
                        "Harare",
                        style: Theme.of(context)
                            .textTheme
                            .headline6!
                            .copyWith(
                            color: primaryColor,
                            fontSize: 13,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),

                  Divider(
                    height: 0,
                    color: Color(0xff7F7F7F),
                  ),

                  Row(
                    children: [
                      Expanded(
                        child: Text(
                          "Posted :",
                          style: Theme.of(context).textTheme.headline6,
                        ),
                      ),
                      const SizedBox(width: defaultPadding),
                      Text(
                        "1400hrs",
                        style: Theme.of(context)
                            .textTheme
                            .headline6!
                            .copyWith(
                            color: primaryColor,
                            fontSize: 13,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),

                  Divider(
                    height: 2,
                    color: Color(0xff7F7F7F),
                  ),

                  Container(
                    height: 58,
                    alignment: Alignment.centerLeft,
                    padding: EdgeInsets.symmetric(horizontal: 24.0),
                    child: Text(
                      'Description',
                      style: Theme.of(context)
                          .textTheme
                          .bodyText2!
                          .copyWith(color: Color(0xff7F7F7F)),
                    ),
                  ),

                  const Padding(
                    padding: EdgeInsets.symmetric(horizontal: 24.5),
                    child: Text(
                      "A Henley shirt is a collarless pullover shirt, by a round neckline and a placket about 3 to 5 inches (8 to 13 cm) long and usually having 2–5 buttons.",
                    ),
                  ),

                 /* Text(
                    "Colors",
                    style: Theme.of(context).textTheme.subtitle2,
                  ),
                  const SizedBox(height: defaultPadding / 2),
                  Row(
                    children: const [
                      ColorDot(
                        color: Color(0xFFBEE8EA),
                        isActive: false,
                      ),
                      ColorDot(
                        color: Color(0xFF141B4A),
                        isActive: true,
                      ),
                      ColorDot(
                        color: Color(0xFFF4E5C3),
                        isActive: false,
                      ),
                    ],
                  ), */

                  SizedBox(
                    height: 10,
                  ),

                  Divider(
                    height: 2,
                    color: Color(0xff7F7F7F),
                  ),


                  const SizedBox(height: defaultPadding * 2),
                  Center(
                    child: SizedBox(
                      width: MediaQuery.of(context).size.width * 0.9,
                      height: 50,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: Color(0xff0ACF83),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                        ),
                        onPressed: () {
                          Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(builder: (context) => ShoppingCartView()),
                          );
                        },
                        child: Text('Add To Cart'),
                      ),
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
