import 'package:flutter/material.dart';

const Color primaryColor = Color(0xff0ACF83);
const Color bgColor = Color(0xFFFBFBFD);

const double defaultPadding = 16.0;
const double defaultBorderRadius = 12.0;

const kTextColor = Color(0xFF535353);
const kTextLightColor = Color(0xFFACACAC);

const kDefaultPadding = 20.0;

const kBorderRadiusMain = 24.0;

const kIconSize = 24.0;
